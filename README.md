# vaytheme

vaytheme is a theme for the [hugo](https://github.com/gohugoio/hugo) static-site generator with a few key objectives:

- no client-side code (JavaScript)
- easily customizable (logic/view separation)
- fully responsive
- multi-language support
- use micro-services server-side to extend functionalities

## TODO

- [ ] Translatable tags (and other taxonomies)
- [ ] [indieweb](https://indieweb.org) support ([microformats2](https://microformats.org) + [webmention](https://indieweb.org/Webmention))
- [ ] Import widgets from [PrettyNoemieCMS](https://framagit.org/framasoft/PrettyNoemieCMS)

## Usage

To use vaytheme, you need to declare it as your theme in your hugo config file:

```
theme="vaytheme"
```

You also need to configure a default language:

```
languageCode = "fr"
DefaultContentLanguage = "fr"
defaultContentLanguageInSubdir = true
```

And then set a `title` and `weight` for this language:

```
[Languages]
[Languages.fr]
title = "CSA Vaydom"
weight = 1
```

See Translating for more information about language management.

## Config

All the theme config is located is `data/vaytheme`. You should not directly edit the config files in your theme folder, but at the root of your website.

For example, to override config values in `themes/vaytheme/data/vaytheme/banner.yaml`, you should create the file `data/vaytheme/banner.yaml`.

> If you don't explicitely override a value in a config file, its default value (provided by the theme) will be used. [More on this](https://gohugo.io/templates/data-templates/#data-files-in-themes) in the hugo docs.

### About me/us (h-card)

The about me/us config is useful to let other websites know who you are and give you proper authorship. `data/vaytheme/about.yaml` :
```
email: "contact@anarch.ism"
```

### Header banner

The default header banner uses the banner widget, which is responsive out-of-the-box. You just need to provide it with images for all screen sizes. To change the default banner, `data/vaytheme/banners/main.yaml` :

```
small: "/images/banner/small.png"
medium: "/images/banner/medium.png"
large: "/images/banner/medium.png"
xlarge: "/images/banner/medium.png"
```

Note that you can reuse the same image for different screens.

### Navigation menu

The default navigation menu uses the menu widget with an horizontal template, that is responsive as well. To edit menu entries for the main nav menu, edit the entries in the `data/vaytheme/menus/main.yaml` file:

```
items:
  - "home"
  - "about": ["location", "history"]
  - "contact"
```

Each entry is found in the translation files in the `i18n/` folder. For instance, the french title for the `location` entry in this default menu called `main` will match the key `main-about-location` in the `i18n/fr.toml` file.

## Layout customization

### Configuring the default layout

The default layout allows you to choose between different templates in some situations. This can be configured in your site config:

```
[params]
[params.vaytheme]
[params.vaytheme.templates]
# summary template (default: "wide") : grid/wide
# Template used for article previews in list
# Write your own in layouts/_default/summary_NAME.html
summary = "wide"
```

### Overriding the default layout

Every page is divided into several widget areas. Each widget area is wrapped in `layouts/partials/base/` while its content is defined directly in the `layouts/partials/` directory.

For example, `layouts/partials/base/header.html` defines the base markup for the header widget area, while `layouts/partials/header.html` defines the actual content within the widget area.

For the moment, three widget areas are provided by default: **header**, **sidebar**, and **footer**.

An example `layouts/partials/base/header.html` may look like this:

```
<header class="header">
    {{/* Calling the modifyable header template */}}
    {{ partial "header.html" . }}
</header>
```

While `layouts/partials/header.html` would look like this:

```
{{/* The banner */}}
{{ $.Scratch.Set "banner" (dict "name" "main" "url" "/") }}
{{ partial "widgets/banner.html" . }}

{{/* Navigation menu */}}
{{ $.Scratch.Set "menu" (dict "name" "nav-main" "template" "h") }}
{{ partial "widgets/menu.html" . }}
```

So, if you'd like to change the base layout of your website, you should look into the `layouts/_default` and `layouts/partials/base` folders. Otherwise, to simply add/edit/remove widgets from a widget area, that's happening directly in the `layouts/partials` folder.

## Widgets

To pass parameters to a widget, we may need to use [Scratch](https://gohugo.io/functions/scratch). In the example before, we did so like this:

```
{{ $.Scratch.Set "banner" (dict "name" "main" "url" "/") }}
```

This is strictly equivalent to:
```
{{ $.Scratch.SetInMap "banner" "name" "main" }}
{{ $.Scratch.SetInMap "banner" "url" "/" }}
```

When you need to pass multiple parameters to a widget, don't hesitate to span them across multiple lines for better readability.

Currently available widgets:

- Banner widget
- Menu widget
- Pillar widget
- Translations widget
- radar widget

## Translating

There are two things you may want to translate on your website. They are the templates (and widgets), and the content.

To enable a new language in our site, we need to add an item in the `Languages` section of our Hugo config. For example, to add the german language, we would do:

```
[Languages]
[Languages.fr]
title = "CSA Vaydom"
weight = 1
[Languages.de]
title = "CSA Vaydom"
weight = 2
```

The `title` property allows to change the site title for a specific language, while the `weight` property changes how soon a language will appear in the translations list.

### Translating templates & widgets

Translations for templates and widgets are found in the `i18n` folder. Currently supported locales are `fr` and `en`. Content provided by default include translations for widgets, dates, and default dummy content.

Translations are simple text files that may look like this:

```
[day_6]
other="Samedi"
[publishdate]
other="02/01/2006"
[readMore]
other="Lire la suite…"
```

To translate stuff to a new language, you need to create a translation file for this language, translate all the strings from either `fr.toml` or `en.toml`, then enable the new language in your hugo config.

So for example, to enable german translations, we would create a `i18n/de.toml` file like this:

```
[day_6]
other="Samstag"
[publishdate]
other="01/02/2006"
[readMore]
other="Mehr lesen…"
```

### Translating your content

To translate your `content/` folder, you need to create for every post one file per language. For instance, `about.fr.md`, `'about.en.md` and `about.de.md` would be the three files for the about page.

They could look something like:

```
---
title: "À propos"
draft: false
---
# Qui sommes-nous ?

Une équipe en carton pour un projet de ouf !
```

Everything that needs to be translated about blog posts and static pages is contained within the content file itself.

If you need, Hugo docs have more information about the possibilities for [translating your content](https://gohugo.io/content-management/multilingual/#translate-your-content).

## External services

### radar_upcoming

Currently, the agenda widget only supports [radar.squat.net](https://radar.squat.net) as a backend. Integration with the radar is made with [radar_upcoming](https://framagit.org/csa-ivry/radar_upcoming) that consumes radar's API and orders events by date.

### what next?

I'm thinking of integrating [webmention.io](https://webmention.io) to allow for social interactions between websites. If you've got any other revolutionary ideas, don't hesitate to suggest :)

## Contributing

Do you have an awesome idea? Are our translations not accurate enough? Can we make it better? **Don't hesitate to contribute ideas, code, and design via Gitlab issues and pull requests.**

Here's a few examples of what you could contribute:

- more/better blog templates
- translations in your language
- integrations with external services
    - iCal-to-JSON API as an agenda backend
    - RSS-to-JSON API to display a feed
    - a nice UI to manage layouts?
- design for an out-of-the-box experience

Do you have an awesome idea? Open an issue or a pull-request :)